FROM jupyter/scipy-notebook:latest

USER root

# debian package
RUN apt update && apt install -y emacs firefox vim curl
RUN apt-get clean -y && apt-get -y autoremove && apt-get -y autoclean

# env
RUN pip install -U pip
COPY requirements.txt /srv/requirements.txt
RUN pip install -r /srv/requirements.txt
RUN rm -f /srv/requirements.txt

# Jupyterlab extensions
RUN pip install -U wheel notebook jupyter jupyterlab jupyterhub ipywidgets
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager @jupyter-voila/jupyterlab-preview

# voila
RUN pip install jupyter-server-proxy
RUN pip install pygments --upgrade
RUN pip install voila

# clean
RUN jlpm cache clean && npm cache clean --force

# workspace (/home/jovyan)
USER jovyan
WORKDIR /home/jovyan/
RUN mkdir /home/jovyan/tool
COPY --chown=jovyan:users tool /home/jovyan/tool

